from datetime import timedelta

from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
from airflow import DAG, macros

import sys
#
# from transform import transform_data
# from generate_output import read


default_args = {
    'owner': 'Groupo_Uno',
    'email': [],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'catchup': False,
    'depends_on_past': True,
    'start_date': days_ago(360),
    'retry_delay': timedelta(minutes=1)
}
dag = DAG(
    'Group1',
    default_args=default_args,
    description='DAG spark spotify-trend',
    schedule_interval='@monthly',
)


########################## requirement
req = """
pip install /root/airflow/dags/spark_spotify_trend-airflow/.
"""

requirement = BashOperator(
    task_id='requirement',
    bash_command=req,
    dag=dag
)


########################## put_py_hdfs
put = """
wget --no-check-certificate https://gitlab.com/Uranium2/spark_spotify_trend/-/raw/master/import_sources.py -O /root/airflow/dags/spark_spotify_trend-airflow/import_sources.py
wget --no-check-certificate https://gitlab.com/Uranium2/spark_spotify_trend/-/raw/master/spotify_api.py -O /root/airflow/dags/spark_spotify_trend-airflow/spotify_api.py
wget --no-check-certificate https://gitlab.com/Uranium2/spark_spotify_trend/-/raw/master/generate_output.py -O /root/airflow/dags/spark_spotify_trend-airflow/generate_output.py
wget --no-check-certificate https://gitlab.com/Uranium2/spark_spotify_trend/-/raw/master/transform.py -O /root/airflow/dags/spark_spotify_trend-airflow/transform.py
wget --no-check-certificate https://gitlab.com/Uranium2/spark_spotify_trend/-/raw/master/utils.py -O /root/airflow/dags/spark_spotify_trend-airflow/utils.py 

hdfs dfs -rm -R -skipTrash /user/iabd2_group1/src/*.py
hdfs dfs -moveFromLocal /root/airflow/dags/spark_spotify_trend-airflow/spotify_api.py /user/iabd2_group1/src/spotify_api.py
hdfs dfs -moveFromLocal /root/airflow/dags/spark_spotify_trend-airflow/generate_output.py /user/iabd2_group1/src/generate_output.py
hdfs dfs -moveFromLocal /root/airflow/dags/spark_spotify_trend-airflow/transform.py /user/iabd2_group1/src/transform.py
hdfs dfs -moveFromLocal /root/airflow/dags/spark_spotify_trend-airflow/utils.py /user/iabd2_group1/src/utils.py
"""


put_py_hdfs = BashOperator(
    task_id='get_and_put_py_hdfs',
    bash_command=put,
    dag=dag
)


########################## import_job
def imports(**kwargs):
    sys.path.append("/root/airflow/dags/spark_spotify_trend-airflow/")
    from import_sources import main
    
    date = kwargs['date']
    print(date)
    main(date)

import_job = PythonOperator(
    task_id='import_job',
    provide_context=True,
    python_callable=imports,
    op_kwargs={'date': "{{ ds }}"}, 
    dag=dag
)


########################## sources_to_hdfs
parq_to_hdfs = """
hdfs dfs -rm -R -skipTrash /user/iabd2_group1/data_raw/date={{ ds }}

ls /root/airflow/dags/spark_spotify_trend-airflow/{{ ds }}.parquet/date={{ ds }}
hdfs dfs -moveFromLocal /root/airflow/dags/spark_spotify_trend-airflow/{{ ds }}.parquet/date={{ ds }} /user/iabd2_group1/data_raw/
"""

sources_to_hdfs = BashOperator(
    task_id='sources_to_hdfs',
    bash_command=parq_to_hdfs,
    dag=dag
)


########################## transform_job
transform_cmd = """
export HADOOP_CONF_DIR=/etc/hadoop/conf && export HADOOP_USER_NAME=iabd2_group1 && spark-submit --master yarn --deploy-mode cluster hdfs://d271ee89-3c06-4d40-b9d6-d3c1d65feb57.priv.instances.scw.cloud/user/iabd2_group1/src/transform.py

"""

transform_job = BashOperator(
    task_id='transform_job',
    bash_command=transform_cmd,
    dag=dag
)


########################## generate_output_job
generate_cmd = """
export HADOOP_CONF_DIR=/etc/hadoop/conf && export HADOOP_USER_NAME=iabd2_group1 && spark-submit --master yarn --deploy-mode cluster hdfs://d271ee89-3c06-4d40-b9d6-d3c1d65feb57.priv.instances.scw.cloud/user/iabd2_group1/src/generate_output.py

"""

generate_output_job = BashOperator(
    task_id='generate_output_job',
    bash_command=generate_cmd,
    dag=dag
)


[requirement >> put_py_hdfs] >> import_job >> sources_to_hdfs >> transform_job >> generate_output_job
