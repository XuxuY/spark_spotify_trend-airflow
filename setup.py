from setuptools import setup, find_packages

print(find_packages())

setup(name='install_reqs',
      version='0.1',
      description='install_reqs for workers',
      long_description="gg la doc",
      url='https://gitlab.com/XuxuY/spark_spotify_trend-airflow',
      author='group1',
      author_email='tavernier.antoine@hotmail.fr',
      packages=find_packages(),
      install_requires=[
          'spotipy',
          'pandas',
          'pyarrow'
      ]
     )
